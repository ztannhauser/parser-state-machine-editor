
#ifndef ENUM_UI_STATE_H
#define ENUM_UI_STATE_H

enum ui_state
{
	us_default,
	us_moving,
	us_dragging,
	us_building_wire,
	
	number_of_ui_states,
};

#endif

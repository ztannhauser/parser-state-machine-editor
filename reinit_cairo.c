
#include "debug.h"

#include "globals/X11.h"
#include "globals/cairo.h"

#include "uninit_cairo.h"
#include "reinit_cairo.h"

int reinit_cairo()
{
	ENTER;
	
	uninit_cairo();
	
	csur = cairo_xlib_surface_create(display, backbuffer, visual, width, height);
	cr = cairo_create(csur);
	cairo_initalizated = true;
	
	EXIT;
	return 0;
}

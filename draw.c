
#include "debug.h"

#include "deps/array/struct.h"

#include "globals/X11.h"
#include "globals/cairo.h"
#include "globals/camera.h"
#include "globals/states.h"
#include "globals/lines.h"
#include "globals/handlers.h"
#include "globals/event_loop.h"

#include "state/struct.h"
#include "state/draw.h"

#include "line/draw.h"

#include "draw.h"

void draw()
{
	size_t i, n;
	ENTER;
	
	XSetForeground(display, gc, -1);
	XFillRectangle(display, backbuffer, gc, 0, 0, width, height);
	
	cairo_identity_matrix(cr);
	cairo_translate(cr, width / 2, height / 2);
	cairo_scale(cr, camera_scale, camera_scale);
	cairo_translate(cr, -camera_x - width / 2, -camera_y - height / 2);
	
	cairo_set_line_width(cr, 2);
	cairo_set_source_rgb(cr, 0, 0, 0);
	cairo_move_to(cr, 0, -1000), cairo_line_to(cr, 0, 1000);
	cairo_move_to(cr, -1000, 0), cairo_line_to(cr, 1000, 0);
	cairo_stroke(cr);
	
	if (ui_state == us_building_wire)
	{
		cairo_set_line_width(cr, 2);
		cairo_set_source_rgb(cr, 0, 0, 0);
		cairo_move_to(cr, state_hovering_on_edge->x, state_hovering_on_edge->y);
		cairo_line_to(cr, mouse_x, mouse_y);
		cairo_stroke(cr);
	}
	
	struct line* lele;
	for (i = 0, n = lines->n; i < n; i++)
	{
		lele = array_index(lines, i, struct line*);
		line_draw(lele);
	}
	
	struct state* sele;
	for (i = 0, n = states->n; i < n; i++)
	{
		sele = array_index(states, i, struct state*);
		state_draw(sele);
	}
	
	
	XdbeSwapBuffers(display, (XdbeSwapInfo[]){{window, XdbeCopied}}, 1);
	
	EXIT;
}



















#include <X11/Xlib.h>
#include <X11/extensions/Xdbe.h>

extern Display* display;
extern int screen;
extern GC gc;
extern Window root_window, window;
extern XdbeBackBuffer backbuffer;
extern Atom wm_delete_window;
extern Visual* visual;
extern int width, height;


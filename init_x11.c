
#include <stdbool.h>

#include "debug.h"

#include "globals/X11.h"

#include "init_x11.h"

int init_x11()
{
	ENTER;
	
	display = XOpenDisplay(NULL);
	
	screen = DefaultScreen(display);
	
	gc = DefaultGC(display, screen);
	
	root_window = RootWindow(display, screen);
	
	visual = DefaultVisual(display, screen);

	window = XCreateSimpleWindow(
		display,
		root_window,
		0, 0,
		width = 100, height = 100,
		0, 0, 0);
	
	backbuffer = XdbeAllocateBackBufferName(display, window, XdbeCopied);
	
	wm_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", false);
	
	XSetWMProtocols(display, window, &wm_delete_window, 1);
	
	XSelectInput(display, window,
		ExposureMask
		| PointerMotionMask
		| StructureNotifyMask
		| ButtonPressMask
		| ButtonReleaseMask);
	
	{
		XSetWindowAttributes attrib = {
			.bit_gravity = NorthWestGravity
		};
		
		XChangeWindowAttributes(display, window, CWBitGravity, &attrib);
	}

	XMapWindow(display, window);
	
	EXIT;
	return 0;
}
















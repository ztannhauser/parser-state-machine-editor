
#include <X11/Xlib.h>

#include "debug.h"

#include "globals/X11.h"
#include "globals/event_loop.h"

#include "default/XExposeEvent.h"
#include "default/XConfigureEvent.h"
#include "default/XButtonPressedEvent.h"
#include "default/XButtonReleasedEvent.h"
#include "default/XMotionEvent.h"

#include "moving/XExposeEvent.h"
#include "moving/XButtonReleasedEvent.h"
#include "moving/XMotionEvent.h"

#include "dragging/XExposeEvent.h"
#include "dragging/XButtonReleasedEvent.h"
#include "dragging/XMotionEvent.h"

#include "building_wire/XExposeEvent.h"
#include "building_wire/XButtonPressedEvent.h"
#include "building_wire/XButtonReleasedEvent.h"
#include "building_wire/XMotionEvent.h"

#include "ui_state.h"
#include "event_loop.h"

static void (*table[number_of_ui_states][LASTEvent])(XEvent*) = 
{
	
	[us_default][Expose] = default_state_XExposeEvent,
	[us_default][ConfigureNotify] = default_state_XConfigureEvent,
	[us_default][ButtonPress] = default_state_XButtonPressedEvent,
	[us_default][ButtonRelease] = default_state_XButtonReleasedEvent,
	[us_default][MotionNotify] = default_state_XMotionEvent,
	
	[us_moving][Expose] = moving_state_XExposeEvent,
	[us_moving][MotionNotify] = moving_state_XMotionEvent,
	[us_moving][ButtonRelease] = moving_state_XButtonReleasedEvent,
	
	[us_dragging][Expose] = dragging_state_XExposeEvent,
	[us_dragging][MotionNotify] = dragging_state_XMotionEvent,
	[us_dragging][ButtonRelease] = dragging_state_XButtonReleasedEvent,
	
	[us_building_wire][Expose] = building_wire_state_XExposeEvent,
	[us_building_wire][MotionNotify] = building_wire_state_XMotionEvent,
	[us_building_wire][ButtonPress] = building_wire_state_XButtonPressedEvent,
	[us_building_wire][ButtonRelease] = building_wire_state_XButtonReleasedEvent,
	
	
};

int event_loop()
{
	XEvent event;
	ENTER;
	
	keep_going = true;
	
	while (keep_going)
	{
		XNextEvent(display, &event);
		
		verpv(event.type);
		
		void (*handler)(XEvent*) = table[ui_state][event.type];
		
		if (handler)
			handler(&event);
	}
	
	EXIT;
	return 0;
}














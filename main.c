
#include <stdio.h>

#include "debug.h"

#include "flags/struct.h"
#include "flags/process_args.h"
#include "flags/delete_flags.h"

#include "init_x11.h"
#include "reinit_cairo.h"
#include "load.h"
#include "event_loop.h"
#include "uninit_cairo.h"
#include "uninit_x11.h"

#if DEBUGGING
int debug_depth;
#endif

int main(int n, const char** args)
{
	int error = 0;
	struct flags* flags = NULL;
	ENTER;
	
	error = process_args(&flags, n, args);
	
	if (!error)
		error = load(flags->directory);
	
	if (!error)
		error = init_x11();
	
	if (!error)
		error = reinit_cairo();
	
	if (!error)
		error = event_loop();
	
	uninit_cairo();
	
	uninit_x11();
	
	delete_flags(flags);
	
	EXIT;
	return error;
}


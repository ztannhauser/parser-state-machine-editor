
#include <cairo/cairo-xlib.h>

#include "debug.h"

#include "globals/cairo.h"

#include "uninit_cairo.h"

int uninit_cairo()
{
	ENTER;
	
	if (cairo_initalizated)
	{
		cairo_destroy(cr);
		cairo_surface_destroy(csur);
		cairo_initalizated = false;
	}
	
	EXIT;
	return 0;
}

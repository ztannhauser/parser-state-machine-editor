

#ifndef DEBUGGING
	#define DEBUGGING (0)
#endif

#if DEBUGGING
	#include <assert.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>
	#include <unistd.h>
	#include <wchar.h>
	#include <stdarg.h>
	#include <string.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <inttypes.h>
	#include <fcntl.h>
	
	extern int debug_depth;
	
	#define D(...) __VA_ARGS__
	#define ND(...)
	
	#define TODO \
	{\
		assert(debug_depth >= 0); \
		printf("%*sTODO: File: %s: Line: %i\n", debug_depth, "", \
			__FILE__, __LINE__);\
		char buffer[100];\
		sprintf(buffer, "+%i", __LINE__);\
		execlp("gedit", "gedit", __FILE__, buffer, NULL);\
		assert(0);\
	}
	
	#define CHECK TODO
	#define NOPE CHECK
	#define HERE \
		printf("%*sHERE: File: %s: Line: %i\n", debug_depth, "", \
			__FILE__, __LINE__);
#else
	#define verpvws(...) ;
	#define D(...)
	#define ND(...) __VA_ARGS__
	#define TODO assert(!"TODO");
	#define CHECK assert(!"CHECK");
	#define NOPE assert(!"NOPE");
	#define HERE ;
#endif


#define ENTER D(assert(debug_depth >= 0), \
	printf("%*s<%s>\n", debug_depth, "", __PRETTY_FUNCTION__), debug_depth++);

#define EXIT  D(debug_depth--, assert(debug_depth >= 0), \
	printf("%*s</%s>\n", debug_depth, "", __PRETTY_FUNCTION__));

#define verpv(val) dprint(val)

#define verprintf(...) \
	D(assert(debug_depth >= 0),\
		printf("%*s", debug_depth, ""), printf(__VA_ARGS__))

#define verpvb(b) \
	D(assert(debug_depth >= 0),\
		printf("%*s" #b " == %s\n", debug_depth, "", (b) ? "true" : "false"))

#define verpvc(ch) \
	D(assert(debug_depth >= 0),\
		printf("%*s" #ch " == '%c'\n", debug_depth, "", ch))

#define verpvs(str) \
	D(assert(debug_depth >= 0),\
		printf("%*s" #str " == \"%s\"\n", debug_depth, "", str))

#define verpvsn(str, len) \
	D(assert(debug_depth >= 0),\
		printf("%*s" #str " == \"%.*s\"\n", debug_depth, "", (int) (len), str))

#define dprint(val) \
	D(assert(debug_depth >= 0),\
		printf("%*s" #val " == ", debug_depth, ""),\
		printf((_Generic(val, \
		bool: "%%b: %i\n", \
		char: "%%c: %i\n", \
		signed char: "%%uc: %u\n", \
		unsigned char: "%%uc: %u\n", \
		signed short: "%%ss: %i\n", \
		unsigned short: "%%us: %u\n", \
		signed int: "%%si: %i\n", \
		unsigned int: "%%ui: %u\n", \
		signed long: "%%sl: %li\n", \
		unsigned long: "%%ul: %lu\n", \
		float: "%%f: %f\n", \
		double: "%%lf: %lf\n", \
		default: "%%p: %p\n")), val))





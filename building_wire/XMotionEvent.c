
#include "debug.h"

#include "globals/handlers.h"
#include "globals/cairo.h"
/*#include "globals/states.h"*/
/*#include "globals/camera.h"*/

/*#include "state/struct.h"*/

#include "draw.h"

#include "XMotionEvent.h"

void building_wire_state_XMotionEvent(XEvent* event)
{
	ENTER;
	
	XMotionEvent* spef = &event->xmotion;
	
	double x = spef->x, y = spef->y;
	cairo_device_to_user(cr, &x, &y);
	
	mouse_x = x, mouse_y = y;
	
	draw();
	
	EXIT;
}

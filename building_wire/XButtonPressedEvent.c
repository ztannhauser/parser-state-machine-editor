
#include <math.h>

#include "debug.h"

#include "deps/array/push_n.h"

#include "ui_state.h"
#include "draw.h"

#include "misc/which_state_is_point_in.h"

#include "globals/event_loop.h"
#include "globals/handlers.h"
#include "globals/cairo.h"
/*#include "globals/states.h"*/
#include "globals/lines.h"

/*#include "state/defines.h"*/
#include "state/struct.h"
#include "state/reorder_input_lines.h"
#include "state/reorder_output_lines.h"
/*#include "state/new.h"*/

#include "line/new.h"

#include "XButtonPressedEvent.h"

void building_wire_state_XButtonPressedEvent(XEvent* event)
{
	ENTER;
	
	XButtonEvent* spef = &event->xbutton;
	
	double x = spef->x, y = spef->y;
	cairo_device_to_user(cr, &x, &y);
	struct state* state = which_state_is_point_in(x, y);
	
	if (state && state_hovering_on_edge != state)
	{
		struct state* start = state_hovering_on_edge;
		struct state* end = state;
		
		struct line* new = new_line(start, end);
		
		array_push_n(start->output_lines, &new);
		array_push_n(end->input_lines, &new);
		
		state_reorder_output_lines(start);
		state_reorder_input_lines(end);
		
		array_push_n(lines, &new);
	}
	
	ui_state = us_default;
	draw();
	
	EXIT;
}




























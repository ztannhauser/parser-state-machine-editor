
#include "debug.h"

#include "struct.h"
#include "new.h"

struct line* new_line(struct state* start, struct state* end)
{
	struct line* this = malloc(sizeof(*this));
	
	this->start = start;
	this->end = end;
	
	return this;
};

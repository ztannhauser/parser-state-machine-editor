
#include <math.h>

#include "deps/array/struct.h"

#include "debug.h"

#include "globals/cairo.h"

#include "state/defines.h"
#include "state/struct.h"

#include "struct.h"
#include "draw.h"

void line_draw(struct line* this)
{
	ENTER;
	
	cairo_set_line_width(cr, 2);
	cairo_set_source_rgb(cr, 0, 0, 0);
	
	verpv(this->start);
	verpv(this->end);
	
	double cs = cos(this->start_angle);
	double ss = sin(this->start_angle);
	
	double ce = cos(this->end_angle);
	double se = sin(this->end_angle);
	
	verpv(this->end_angle);
	
	if (this->start->x + STATE_RADIUS < this->end->x - STATE_RADIUS)
	{
		cairo_move_to(cr,
			this->start->x + cs * STATE_RADIUS,
			this->start->y + ss * STATE_RADIUS);
		
		cairo_curve_to(cr,
			this->start->x + cs * STATE_RADIUS * 2,
			this->start->y + ss * STATE_RADIUS * 2,
			this->end->x + ce * STATE_RADIUS * 2,
			this->end->y + se * STATE_RADIUS * 2,
			this->end->x + ce * STATE_RADIUS,
			this->end->y + se * STATE_RADIUS);
	}
	else
	{
		double mid_x = (this->start->x + this->end->x) / 2;
		double mid_y = (this->start->y + this->end->y) / 2;
		
		cairo_move_to(cr,
			this->start->x + cs * STATE_RADIUS,
			this->start->y + ss * STATE_RADIUS);
		
		cairo_curve_to(cr,
			this->start->x + cs * STATE_RADIUS * 2,
			this->start->y + ss * STATE_RADIUS * 2,
			mid_x + STATE_RADIUS * 2, mid_y,
			mid_x, mid_y);
			
		cairo_curve_to(cr,
			mid_x - STATE_RADIUS * 2, mid_y,
			this->end->x + ce * STATE_RADIUS * 2,
			this->end->y + se * STATE_RADIUS * 2,
			this->end->x + ce * STATE_RADIUS,
			this->end->y + se * STATE_RADIUS);
	}
	
	cairo_stroke(cr);
	
	EXIT;
};

































#include "debug.h"

#include "draw.h"

#include "XExposeEvent.h"

void default_state_XExposeEvent(XEvent* event)
{
	ENTER;
	draw();
	EXIT;
}


#include "debug.h"

#include "deps/array/push_n.h"

#include "globals/event_loop.h"
#include "globals/camera.h"

/*#include "globals/event_loop.h"*/
/*#include "globals/handlers.h"*/
/*#include "globals/cairo.h"*/
/*#include "globals/filedata.h"*/

/*#include "state/new.h"*/

#include "draw.h"

#include "XButtonReleasedEvent.h"

#define CAMERA_SCALE_FACTOR (95.0 / 100.0)

void default_state_XButtonReleasedEvent(XEvent* event)
{
	ENTER;
	
	XButtonEvent* spef = &event->xbutton;
	
	switch (spef->button)
	{
		case Button1:
		{
			break;
		}
		
		case Button4:
		{
			camera_scale /= CAMERA_SCALE_FACTOR;
			draw();
			break;
		}
		
		case Button5:
		{
			camera_scale *= CAMERA_SCALE_FACTOR;
			draw();
			break;
		}
	}
	
	EXIT;
}


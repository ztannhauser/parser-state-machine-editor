
#include "debug.h"

#include "globals/X11.h"

#include "reinit_cairo.h"

#include "XConfigureEvent.h"

void default_state_XConfigureEvent(XEvent* event)
{
	ENTER;
	
	XConfigureEvent* spef = &event->xconfigure;
	
	width = spef->width, height = spef->height;
	
	reinit_cairo();
	
	EXIT;
}


#include <math.h>

#include "debug.h"

#include "deps/array/push_n.h"

#include "ui_state.h"
#include "draw.h"

#include "misc/which_state_is_point_in.h"

#include "globals/event_loop.h"
#include "globals/handlers.h"
#include "globals/cairo.h"
#include "globals/states.h"

#include "state/defines.h"
#include "state/struct.h"
#include "state/new.h"

#include "XButtonPressedEvent.h"

void default_state_XButtonPressedEvent(XEvent* event)
{
	ENTER;
	
	XButtonEvent* spef = &event->xbutton;
	
	switch (spef->button)
	{
		case Button1:
		{
			struct state* state, *ele;
			size_t i, n;
			double x, y;
			
			if (state_hovering_on_edge)
			{
				ui_state = us_building_wire;
				mouse_x = state_hovering_on_edge->x;
				mouse_y = state_hovering_on_edge->y;
				draw();
			}
			else if (
				x = spef->x, y = spef->y,
				cairo_device_to_user(cr, &x, &y),
				state = which_state_is_point_in(x, y),
				state && ControlMask & spef->state)
			{
				state->is_selected = !state->is_selected;
				
				draw();
			}
			else if (state && state->is_selected)
			{
				ui_state = us_dragging;
				mouse_button_down_x = spef->x;
				mouse_button_down_y = spef->y;
				
				draw();
			}
			else if (state)
			{
				for (i = 0, n = states->n; i < n; i++)
					ele = array_index(states, i, struct state*),
					ele->is_selected = false;
					
				state->is_selected = true;
				ui_state = us_dragging;
				mouse_button_down_x = spef->x;
				mouse_button_down_y = spef->y;
				
				draw();
			}
			else if (spef->time - last_button1_down_time < 500)
			{
				double x = spef->x, y = spef->y;
				cairo_device_to_user(cr, &x, &y);
				struct state* new = new_state(x, y);
				array_push_n(states, &new);
				draw();
			}
			else
			{
				for (i = 0, n = states->n; i < n; i++)
					ele = array_index(states, i, struct state*),
					ele->is_selected = false;
				
				draw();
				
				last_button1_down_time = spef->time;
			}
			
			break;
		}
		
		case Button2:
		{
			mouse_button_down_x = spef->x;
			mouse_button_down_y = spef->y;
			ui_state = us_moving;
			break;
		}
	}
	
	EXIT;
}


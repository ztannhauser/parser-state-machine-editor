
#include <math.h>
#include <X11/cursorfont.h>

#include "debug.h"

#include "deps/array/struct.h"

#include "globals/X11.h"
#include "globals/cairo.h"
#include "globals/states.h"
#include "globals/handlers.h"

#include "state/defines.h"
#include "state/struct.h"

#include "XMotionEvent.h"

static struct state* which_state_edge_is_point_in(double x, double y)
{
	struct state* ret = NULL, *ele;
	size_t i, n;
	double dist;
	ENTER;
	
	for (i = 0, n = states->n; !ret && i < n; i++)
	{
		ele = array_index(states, i, struct state*);
		
		dist = hypot(ele->x - x, ele->y - y) - STATE_RADIUS;
		
/*		if (fabs( - STATE_RADIUS) < STATE_LINE_THICKNESS / 2)*/
/*		if (0 < dist && dist < STATE_LINE_THICKNESS)*/
		if (0 < dist && dist < STATE_LINE_THICKNESS/2)
/*		if (-STATE_LINE_THICKNESS < dist && dist < 0)*/
		{
			ret = ele;
		}
	}
	
	EXIT;
	return ret;
}

static Cursor pencil, normal;
static bool have_created_font_cursors = false;

void default_state_XMotionEvent(XEvent* event)
{
	ENTER;
	
	XMotionEvent* spef = &event->xmotion;
	
	verpv(spef->x);
	verpv(spef->y);
	
	double x = spef->x, y = spef->y;
	
	cairo_device_to_user(cr, &x, &y);
	
	struct state* state = which_state_edge_is_point_in(x, y);
	
	if (!have_created_font_cursors)
	{
		pencil = XCreateFontCursor(display, XC_pencil);
		normal = XCreateFontCursor(display, XC_arrow);
		have_created_font_cursors = true;
	}
	
	if (!state_hovering_on_edge != !state)
		XDefineCursor(display, window, state ? pencil : normal);
	
	state_hovering_on_edge = state;
	
	EXIT;
}
















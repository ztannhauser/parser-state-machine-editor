
#include "debug.h"

#include "globals/handlers.h"
#include "globals/states.h"
#include "globals/camera.h"

#include "state/struct.h"
#include "state/reorder_input_lines.h"
#include "state/reorder_output_lines.h"

#include "line/struct.h"

#include "draw.h"

#include "XMotionEvent.h"

void dragging_state_XMotionEvent(XEvent* event)
{
	size_t i, n, j, m;
	struct state* state;
	struct line* line;
	ENTER;
	
	XMotionEvent* spef = &event->xmotion;
	
	double delta_x = (spef->x - mouse_button_down_x) / camera_scale;
	double delta_y = (spef->y - mouse_button_down_y) / camera_scale;
	
	verpv(delta_x);
	verpv(delta_y);
	
	for (i = 0, n = states->n; i < n; i++)
	{
		state = array_index(states, i, struct state*);
		if (state->is_selected)
		{
			state->x += delta_x;
			state->y += delta_y;
			
			for (j = 0, m = state->input_lines->n; j < m; j++)
			{
				line = array_index(state->input_lines, j, struct line*);
				
				state_reorder_output_lines(line->start);
			}
			
			for (j = 0, m = state->output_lines->n; j < m; j++)
			{
				line = array_index(state->output_lines, j, struct line*);
				
				state_reorder_input_lines(line->end);
			}
		}
	}
	
	mouse_button_down_x = spef->x;
	mouse_button_down_y = spef->y;
	
	draw();
	
	EXIT;
}


















#include "debug.h"

#include "globals/event_loop.h"

#include "XButtonReleasedEvent.h"

void dragging_state_XButtonReleasedEvent(XEvent* event)
{
	ENTER;
	
	ui_state = us_default;
	
	EXIT;
}


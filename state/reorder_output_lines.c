
#include <math.h>

#include "deps/array/struct.h"

#include "debug.h"

#include "line/struct.h"

#include "struct.h"
#include "reorder_output_lines.h"

static int compare(const void* a, const void* b)
{
	int cmp = 0;
	ENTER;
	
	struct line** A = (struct line**) a;
	struct line** B = (struct line**) b;
	
	double a_y = (*A)->end->y;
	double b_y = (*B)->end->y;
	
	if (a_y > b_y)
		cmp = 1;
	else if (a_y < b_y)
		cmp = -1;
	
	EXIT;
	return cmp;
}

void state_reorder_output_lines(struct state* this)
{
	ENTER;
	
	qsort(
		this->output_lines->data,
		this->output_lines->n,
		this->output_lines->elesize,
		compare);
	
	size_t i, n;
	struct line* ele;
	for (i = 0, n = this->output_lines->n; i < n; i++)
	{
		ele = array_index(this->output_lines, i, struct line*);
		ele->start_angle = M_PI * (i + 1) / (n + 1) - M_PI_2;
	}
	
	
	EXIT;
}























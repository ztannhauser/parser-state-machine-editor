
#include <math.h>

#include "deps/array/struct.h"

#include "debug.h"

#include "line/struct.h"

#include "struct.h"
#include "reorder_output_lines.h"

static int compare(const void* a, const void* b)
{
	int cmp = 0;
	ENTER;
	
	struct line** A = (struct line**) a;
	struct line** B = (struct line**) b;
	
	double a_y = (*A)->start->y;
	double b_y = (*B)->start->y;
	
	if (a_y > b_y)
		cmp = 1;
	else if (a_y < b_y)
		cmp = -1;
	
	EXIT;
	return cmp;
}

void state_reorder_input_lines(struct state* this)
{
	ENTER;
	
	qsort(
		this->input_lines->data,
		this->input_lines->n,
		this->input_lines->elesize,
		compare);
	
	size_t i, n;
	struct line* ele;
	for (i = 0, n = this->input_lines->n; i < n; i++)
	{
		ele = array_index(this->input_lines, i, struct line*);
		ele->end_angle = -M_PI * (i + 1) / (n + 1) - M_PI_2;
	}
	
	
	EXIT;
}
























#include <stdlib.h>

#include "deps/array/new.h"

#include "debug.h"

#include "struct.h"
#include "new.h"

struct state* new_state(double x, double y)
{
	struct state* this;
	ENTER;
	
	this = malloc(sizeof(*this));
	
	verpv(this);
	
	this->x = x, this->y = y;
	
	this->is_selected = false;
	
	verpv(this->x);
	verpv(this->y);
	
	this->input_lines = new_array(struct line*);
	this->output_lines = new_array(struct line*);
	
	EXIT;
	return this;
}

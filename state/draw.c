
#include <math.h>

#include "debug.h"

#include "globals/cairo.h"

#include "struct.h"
#include "defines.h"
#include "draw.h"

void state_draw(struct state* this)
{
	ENTER;
	
	cairo_set_line_width(cr, STATE_LINE_THICKNESS);
	
	verpv(this->x);
	verpv(this->y);
	
	#if 0
	cairo_set_source_rgb(cr, 0, 0, 0);
	
	cairo_move_to(cr, this->x, this->y);
	
	cairo_rel_line_to(cr, 2 * STATE_RADIUS, 2 * STATE_RADIUS);
	
	cairo_move_to(cr, this->x, this->y);
	
	cairo_rel_line_to(cr, STATE_RADIUS * 2 * M_SQRT2, 0);
	
	cairo_move_to(cr, this->x, this->y);
	
	cairo_rel_line_to(cr, 2 * STATE_RADIUS, - 2 * STATE_RADIUS);
	
	cairo_stroke(cr);
	#endif
	
	if (this->is_selected)
		cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
	else
		cairo_set_source_rgb(cr, 0, 0, 0);
	
	cairo_arc(cr, this->x, this->y, STATE_RADIUS, 0, 2 * M_PI);
	
	cairo_stroke_preserve(cr);
	
	cairo_set_source_rgb(cr, 1, 1, 1);
	
	cairo_fill(cr);
	
	
	EXIT;
}


struct array;

struct state
{
	double x, y;
	bool is_selected;
	
	struct array* input_lines;
	
	struct array* output_lines;
	
};

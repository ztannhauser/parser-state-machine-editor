
#include "struct.h"

void* array_bsearch(
	struct array* this,
	void* ele,
	int (*compar)(const void *, const void *)
)
	__attribute__ ((warn_unused_result));

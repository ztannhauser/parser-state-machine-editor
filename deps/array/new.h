

#define new_arrayp() new_array(void*)

#define new_array(type) new_array_given_size(sizeof(type))

struct array* new_array_given_size(int elesize);


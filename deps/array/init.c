
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "struct.h"

#include "init.h"

struct array *array_init(struct array *this, int elesize) {
	this->n = 0;
	this->data = NULL;
	this->mem_length = 0;
	this->elesize = elesize;
	return this;
}

struct array *array_init_given_data(struct array *this, const void *data,
									unsigned long n, int elesize) {
	this->n = n;
	this->mem_length = n * elesize;
	this->data = memcpy(malloc(this->mem_length), data, this->mem_length);
	this->elesize = elesize;
	return this;
}

#include <stdlib.h>

#include "struct.h"

#include "delete.h"

void delete_array(struct array *a) {
	free(a->data);
}

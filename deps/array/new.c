
#include <stdio.h>

#include "debug.h"

#include "init.h"
#include "struct.h"
#include "new.h"

struct array* new_array_given_size(int elesize)
{
	struct array* this;
	ENTER;
	
	this = malloc(sizeof(*this));
	array_init(this, elesize);
	
	EXIT;
	return this;
}


#include <stdlib.h>

#include "struct.h"

#include "bsearch.h"

void *array_bsearch(struct array *this, void *ele,
					int (*compar)(const void *, const void *)) {
	return bsearch(ele, this->data, this->n, this->elesize, compar);
}

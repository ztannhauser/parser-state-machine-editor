
#include <assert.h>
#include <stdlib.h>

#include "struct.h"

#include "free_elements.h"

void array_free_elements(struct array *a) {
	for (unsigned long i = 0; i < a->n; i++) {
		free(a->datavp[i]);
	}
}

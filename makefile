
CC = gcc

CFLAGS += -Wall -Werror

CPPFLAGS += -I .
CPPFLAGS += -D DEBUGGING=1

LDLIBS += -lX11 -lXext -lcairo -lm

default: bin/psme

bin:
	mkdir -p bin

srclist.mk:
	find -name '*.c' ! -path './examples*' | sed 's/^/SRCS += /' > $@

include srclist.mk

OBJS = $(SRCS:.c=.o)
DEPENDS = $(SRCS:.c=.mk)

%.mk: %.c
	$(CPP) $(CPPFLAGS) $< -MM -MF $@ -MT $@ || (gedit $<; false)

include $(DEPENDS)

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ || (gedit $<; false)

bin/psme: $(OBJS) | bin
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

run: bin/psme
	bin/psme examples/json

valrun: bin/psme
	valgrind bin/psme examples/json

clean:
	find -name '*.o' -delete

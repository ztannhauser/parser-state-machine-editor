
enum token
{
	t_open_curly,  t_close_curly,
	t_open_square, t_close_square,
	
	t_numeric_literal,
	t_string_literal,
	
	t_false,
	t_true,
	t_null,
	
	t_colon,
	t_comma,
	
	number_of_tokens,
};


enum tokens
{
	t_error,
	
	t_number,
	t_plus,
	t_minus,
	t_times,
	t_divide,
	
	number_of_tokens,
};


#include "debug.h"

#include "globals/handlers.h"
#include "globals/camera.h"

#include "draw.h"

#include "XMotionEvent.h"

void moving_state_XMotionEvent(XEvent* event)
{
	ENTER;
	
	XMotionEvent* spef = &event->xmotion;
	
	int delta_x = spef->x - mouse_button_down_x;
	int delta_y = spef->y - mouse_button_down_y;
	
	verpv(delta_x);
	verpv(delta_y);
	
	camera_x -= delta_x / camera_scale, mouse_button_down_x += delta_x;
	camera_y -= delta_y / camera_scale, mouse_button_down_y += delta_y;
	
	verpv(camera_x);
	verpv(camera_y);
	
	draw();
	
	EXIT;
}

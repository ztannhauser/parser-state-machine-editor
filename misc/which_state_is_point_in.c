
#include <math.h>

#include "debug.h"

#include "globals/states.h"

#include "state/defines.h"
#include "state/struct.h"

#include "which_state_is_point_in.h"

struct state* which_state_is_point_in(double x, double y)
{
	struct state* ret = NULL, *ele;
	size_t i, n;
	ENTER;
	
	for (i = 0, n = states->n; !ret && i < n; i++)
	{
		ele = array_index(states, i, struct state*);
		if (hypot(ele->x - x, ele->y - y) < STATE_RADIUS)
		{
			ret = ele;
		}
	}
	
	EXIT;
	return ret;
}


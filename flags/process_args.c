
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "debug.h"

#include "struct.h"
#include "process_args.h"

int process_args(struct flags** flags_out, int n, const char** args)
{
	int error = 0;
	ENTER;
	
	if (n != 2)
	{
		printf("bad number of params!\n");
		error = 1;
	}
	
	if (!error)
	{
		struct flags* flags = malloc(sizeof(*flags));
		
		strcpy(flags->directory, args[1]);
		
		*flags_out = flags;
	}
	
	EXIT;
	return error;
}

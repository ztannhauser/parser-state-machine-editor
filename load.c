
#include "debug.h"

#include "deps/array/new.h"
#include "deps/array/push_n.h"

#include "globals/camera.h"
#include "globals/lines.h"
#include "globals/states.h"

#include "state/struct.h"
#include "state/new.h"

#include "load.h"

int load(const char* directory)
{
	ENTER;
	
	camera_x = 0, camera_y = 0, camera_scale = 1.0;
	
	states = new_array(struct state*);
	lines = new_array(struct line*);
	
	{
		struct state* start_state = new_state(0, 0);
		array_push_n(states, &start_state);
	}
	
	EXIT;
	return 0;
}
